// ip-transforms.test.js

const { ipToLong, longToIp } = require('./ip-transforms');

describe('ip-transforms', () => {
    // Test the ipToLong function
    describe('ipToLong', () => {
        // Test that ipToLong correctly converts a dotted IPv4 address to a 32-bit signed long integer
        test('converts a dotted IPv4 address to a 32-bit signed long integer', () => {
            expect(ipToLong('192.168.1.1')).toBe(-1062731519);
            expect(ipToLong('0.0.0.0')).toBe(0);
            expect(ipToLong('255.255.255.255')).toBe(-1);
        });

        // Test that ipToLong throws an error for invalid input
        test('throws an error for invalid input', () => {
            expect(() => ipToLong('192.168.1')).toThrow('Invalid IP address');
            expect(() => ipToLong('192')).toThrow('Invalid IP address');
            expect(() => ipToLong('192.168')).toThrow('Invalid IP address');
            expect(() => ipToLong('not an ip address')).toThrow('Invalid IP address');
        });
    });

    // Test the longToIp function
    describe('longToIp', () => {
        // Test that longToIp correctly converts a 32-bit signed long integer to a dotted IPv4 address
        test('converts a 32-bit signed long integer to a dotted IPv4 address', () => {
            expect(longToIp(-1062731519)).toBe('192.168.1.1');
            expect(longToIp(0)).toBe('0.0.0.0');
            expect(longToIp(-1)).toBe('255.255.255.255');
        });

        // Test that longToIp throws an error for invalid input
        test('throws an error for invalid input', () => {
            expect(() => longToIp("")).toThrow('Invalid long integer');
            expect(() => longToIp('not a long integer')).toThrow('Invalid long integer');
        });
    });
});