// ip-transforms.js

/**
 * This function converts a dotted IPv4 address to a 32-bit signed long integer.
 * @param {string} ip - The dotted IPv4 address.
 * @returns {number} - The 32-bit signed long integer representation of the IP address.
 * @throws {Error} - Throws an error if the input is not a valid dotted IPv4 address.
 */
exports.ipToLong = (ip) => {
  if (typeof ip !== 'string' || !/^(\d{1,3}\.){3}\d{1,3}$/.test(ip)) {
      throw new Error('Invalid IP address');
  }

  //Splits the ip into it's components to be joined later
  const octets = ip.split('.');

  //Makes sure all octets are 8-bit
  if (octets.some(octet => octet < 0 || octet > 255)) {
      throw new Error('Invalid IP address');
  }


  //load octets with bitwise operators.
  let long = ((octets[0] << 24) | (octets[1] << 16) | (octets[2] << 8) | octets[3]);

  //if the result is negative, take the twos compliment before returning.
  if (long >>> 31 === 0) {
    return long;
  } else {
    return -((~long & 0xFFFFFFFF) + 1);
  }
}

/**
* This function converts a 32-bit signed long integer to a dotted IPv4 address.
* @param {number} long - The 32-bit signed long integer.
* @returns {string} - The dotted IPv4 address.
* @throws {Error} - Throws an error if the input is not a valid 32-bit signed long integer.
*/
exports.longToIp = (long) => {
  if (typeof long !== 'number' || long < -2147483648 || long > 2147483647) {
      throw new Error('Invalid long integer');
  }

  if (long < 0) {
      long = (~Math.abs(long) + 1) >>> 0;
  }

  const octets = [
      (long >>> 24) & 0xFF,
      (long >>> 16) & 0xFF,
      (long >>> 8) & 0xFF,
      long & 0xFF
  ];

  return octets.join('.');
}