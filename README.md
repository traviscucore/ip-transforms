# IP Transforms

This Node.js module provides two functions for converting between dotted IPv4 addresses and 32-bit signed long integers.

## Functions

### ipToLong(ip)

Converts a dotted IPv4 address to a 32-bit signed long integer.

#### Parameters

- `ip` (string): The dotted IPv4 address.

#### Returns

- (number): The 32-bit signed long integer representation of the IP address.

#### Throws

- (Error): If the input is not a valid dotted IPv4 address.

### longToIp(long)

Converts a 32-bit signed long integer to a dotted IPv4 address.

#### Parameters

- `long` (number): The 32-bit signed long integer.

#### Returns

- (string): The dotted IPv4 address.

#### Throws

- (Error): If the input is not a valid 32-bit signed long integer.

## Installation

```bash
npm install ip-transforms
